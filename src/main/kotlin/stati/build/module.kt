package stati.build

import mu.KotlinLogging.logger
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import stati.KodeinModules
import stati.Konfig
import stati.build.adapter.DummyBuildFetcher
import stati.build.adapter.selenium.SeleniumBuildFetcher
import stati.build.adapter.selenium.SeleniumKonfig
import stati.build.domain.BuildFetcher
import stati.build.domain.BuildRetriever
import stati.build.domain.BuildService
import stati.build.domain.NotifierService

private val log = logger {}

@Suppress("unused") fun KodeinModules.buildModule(konfig: Konfig) = Kodein.Module("Build Module") {
    bind<NotifierService>() with singleton { NotifierService(instance(), instance(), instance(), instance()) }
    bind<BuildService>() with singleton { BuildService(instance(), instance()) }
    bind<BuildRetriever>() with singleton {
        BuildRetriever(
            projects = konfig.projects,
            fetcher = instance()
        )
    }
    if (konfig.enableSeleniumBuildFetcher) {
        bind<BuildFetcher>() with singleton {
            SeleniumBuildFetcher(
                konfig = SeleniumKonfig(
                    username = konfig.googleUsername,
                    password = konfig.googlePassword,
                    headless = konfig.seleniumHeadless
                ),
                projects = konfig.projects,
                ignoredSources = konfig.ignoredBuildSources
            )
        }
    } else {
        log.warn { "[DEVELOP] Binding ${DummyBuildFetcher::class.simpleName}" }
        bind<BuildFetcher>() with instance(DummyBuildFetcher)
    }
}
