package stati.build.domain

import stati.build.adapter.selenium.FetchUntil

interface BuildFetcher {
    fun fetchAll(until: FetchUntil): List<BuildStatus>
}

