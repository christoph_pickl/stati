package stati.build.domain

import stati.domain.ProjectId

class BuildService(
    private val retriever: BuildRetriever,
    private val notifier: NotifierService
) {

    private val previousBuildStatus = mutableMapOf<ProjectId, BuildStatus>()

    fun fetchReport(): BuildReport {
        val stati = retriever.retrieve()

        val mostRecentBuilds = stati.map.values.mapNotNull { it.lastOrNull() }
        val changed = calcChanged(mostRecentBuilds)

        if (changed.isNotEmpty()) {
            notifier.notify(changed)
        }
        return BuildReport(
            stati = stati.map {
                if (it.second.isEmpty()) {
                    BuildReportLine.unknown(it.first)
                } else {
                    val recentBuild = it.second.last()
                    BuildReportLine(
                        projectId = it.first,
                        buildId = recentBuild.buildId,
                        buildUrl = recentBuild.buildUrl,
                        status = recentBuild.status,
                        lastSuccess = it.second.lastOrNull { it.status == BuildStatusIndicator.Success }?.started,
                        duration = recentBuild.duration
                    )
                }
            },
            changed = changed
        )
    }

    private fun calcChanged(stati: List<BuildStatus>): List<BuildReportChange> {
        val changed = mutableListOf<BuildReportChange>()
        stati.forEach { status ->
            val storedStatus = previousBuildStatus[status.projectId]
            if (storedStatus != null && storedStatus.status != status.status) {
                changed += BuildReportChange(
                    projectId = status.projectId,
                    oldStatus = storedStatus.status,
                    newStatus = status.status
                )
            }
            previousBuildStatus[status.projectId] = status
        }
        return changed
    }
}
