package stati.build.domain

import stati.Konfig
import stati.domain.Email
import stati.domain.EmailSender
import stati.domain.GitView
import stati.domain.NotificationNotifier

class NotifierService(
    private val konfig: Konfig,
    private val growl: NotificationNotifier,
    private val email: EmailSender,
    private val git: GitView
) {
    fun notify(changed: List<BuildReportChange>) {

        val failed = changed.filter { it.newStatus == BuildStatusIndicator.Failing }
        val succeeded = changed.filter { it.newStatus == BuildStatusIndicator.Success }

        //@formatter:off
        val message = (
            (if (failed.isEmpty()) emptyList() else listOf("Failed projects: ${failed.joinToString { konfig.projectById(it.projectId).label }}")) +
            (if (succeeded.isEmpty()) emptyList() else listOf("Succeeded projects: ${succeeded.joinToString { konfig.projectById(it.projectId).label }}"))
        ).joinToString("; ")
        //@formatter:on
        growl.notify(message)


        failed.forEach { fail ->
            val project = konfig.projectById(fail.projectId)
//            git.allCommittersSince(project, CommitId(""))
            email.send(Email(
                receivers = listOf("christoph.pickl@ibood.com"),
                subject = "Build failed for: ${project.label}",
                body = "Check this URL: ... TODO ..."
            ))
        }
    }

}
