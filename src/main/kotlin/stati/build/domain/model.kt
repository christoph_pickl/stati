package stati.build.domain

import stati.domain.HasProjectId
import stati.domain.ProjectId
import java.time.Duration
import java.time.LocalDateTime

typealias Url = String

data class BuildId(val id: String)

data class BuildStatus(
    override val projectId: ProjectId,
    val buildId: BuildId,
    val buildUrl: Url,
    val status: BuildStatusIndicator,
    val started: LocalDateTime,
    val duration: Duration?
//    val gitCommit: String,
) : HasProjectId

data class BuildReport(
    val stati: List<BuildReportLine>,
    val changed: List<BuildReportChange>
)

data class BuildReportLine(
    override val projectId: ProjectId,
    val buildId: BuildId,
    val status: BuildStatusIndicator,
    val lastSuccess: LocalDateTime?,
    val duration: Duration?,
    val buildUrl: Url?
) : HasProjectId {
    companion object {
        fun unknown(projectId: ProjectId) = BuildReportLine(
            projectId = projectId,
            buildId = BuildId("?"),
            status = BuildStatusIndicator.Unknown,
            lastSuccess = null,
            duration = null,
            buildUrl = null
        )
    }
}

data class BuildReportChange(
    override val projectId: ProjectId,
    val oldStatus: BuildStatusIndicator,
    val newStatus: BuildStatusIndicator
) : HasProjectId {
    init {
        require(oldStatus != newStatus)
    }
}

enum class BuildStatusIndicator {
    Success,
    Failing,
    Building,
    Stopped,
    Unknown;
    companion object // for extensions
}
