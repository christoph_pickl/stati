package stati.build.domain

import mu.KotlinLogging.logger
import stati.build.adapter.selenium.FetchUntil
import stati.domain.IProjectMap
import stati.domain.ProjectDefinition
import stati.domain.ProjectId
import stati.domain.ProjectMap

//fun main() {
//    val log = logger {}
//    val projects = listOf(Konfig.production.projects.first { it.projectId.id == "aloha" })
//    val retriever = BuildRetriever(
//        projects = projects,
//        fetcher = SeleniumBuildRowFetcher(
//            konfig = SeleniumKonfig(
//                username = Konfig.production.googleUsername,
//                password = Konfig.production.googlePassword,
//                headless = true
//            ),
//            projects = projects
//        )
//    )
//    val stati = retriever.retrieve()
//    log.info { "stati.size: ${stati.size}" }
//    log.info { "stati: ${(stati.values.joinToString("\n"))}" }
//}

class BuildRetriever(
    private val fetcher: BuildFetcher,
    private val projects: List<ProjectDefinition>
) {

    private val log = logger {}
    private var recentBuildId: BuildId? = null

    private val statiByProjectId: MutableMap<ProjectId, MutableList<BuildStatus>> = mutableMapOf(
        *projects.map { it.projectId to mutableListOf<BuildStatus>() }.toTypedArray()
    )

    fun retrieve(): IProjectMap<List<BuildStatus>> {
        update()
        return statiByProjectId.toProjectMap()
    }

    private fun update() {
        val rows = recentBuildId?.let {
            fetcher.fetchAll(FetchUntil.Until(it))
        } ?: fetcher.fetchAll(FetchUntil.All(projects))
        log.trace { "Fetched ${rows.size} rows." }

        rows.lastOrNull()?.let {
            log.trace { "Saving most recent build ID: ${it.buildId.id}" }
            recentBuildId = it.buildId
        }

        rows.groupBy { it.projectId }.forEach { (projectId, rows) ->
            statiByProjectId[projectId]!!.addAll(rows.map { row ->
                BuildStatus(
                    projectId = row.projectId,
                    buildId = row.buildId,
                    buildUrl = row.buildUrl,
                    status = row.status,
                    started = row.started,
                    duration = row.duration
                )
            })
        }
    }
}

private fun <V> Map<ProjectId, List<V>>.toProjectMap(): IProjectMap<List<V>> = ProjectMap.buildById(
    map { it.key to it.value }.toMap()
)
