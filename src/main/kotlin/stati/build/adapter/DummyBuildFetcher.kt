package stati.build.adapter

import mu.KotlinLogging.logger
import stati.Konfig
import stati.build.adapter.selenium.FetchUntil
import stati.build.adapter.selenium.GOOGLE_BASE_URL
import stati.build.domain.BuildFetcher
import stati.build.domain.BuildId
import stati.build.domain.BuildStatus
import stati.build.domain.BuildStatusIndicator
import java.time.Duration
import java.time.LocalDateTime

object DummyBuildFetcher : BuildFetcher {
    private val log = logger {}
    private var fetchCounter = 0

    override fun fetchAll(until: FetchUntil): List<BuildStatus> {
        log.info { "fetchAll($until)" }
        fetchCounter++
        var projectCounter = 0
//        if (fetchCounter == 1) {
//            throw Exception("BANG!")
//        }
        Thread.sleep(2_000)
        return Konfig.production.projects.take(4).map {
            projectCounter++
            BuildStatus(
                projectId = it.projectId,
                buildId = BuildId("buildId$fetchCounter/$projectCounter"),
                started = LocalDateTime.now().minusMinutes(fetchCounter.toLong() * 60 + projectCounter * 30L),
                buildUrl = GOOGLE_BASE_URL + "/cloud-build/builds/c58a975f-7176-4891-81ce-fbae19db580c?project=sandbox-ijsberen&authuser=1",
                status = when (fetchCounter) {
                    1 -> BuildStatusIndicator.Success
                    2 -> {
                        if (projectCounter == 1 || projectCounter == 2) {
                            BuildStatusIndicator.Failing
                        } else {
                            BuildStatusIndicator.Success
                        }
                    }
                    else -> BuildStatusIndicator.Success
                },
                duration = Duration.ofMinutes(projectCounter * 2L + fetchCounter)
            )
        }
    }
}
