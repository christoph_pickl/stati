package stati.build.adapter.selenium

import mu.KotlinLogging.logger
import stati.build.domain.BuildId
import stati.build.domain.BuildStatus
import stati.domain.ProjectDefinition

sealed class FetchUntil : Iterable<Int> {

    abstract fun feedNextPage(nextRows: List<BuildStatus>)
    abstract fun clean(rows: List<BuildStatus>): List<BuildStatus>

    class All(
        projects: List<ProjectDefinition>,
        private val maxPages: Int = 3
    ) : FetchUntil() {

        private val log = logger {}
        private var currentPage = 1
        private val projectsLeft = projects.toMutableSet()

        override fun iterator(): Iterator<Int> = object : AbstractIterator<Int>() {
            override fun computeNext() {
                when {
                    currentPage > maxPages -> {
                        log.trace { "Max pages fetched." }
                        done()
                    }
                    projectsLeft.isEmpty() -> {
                        log.trace { "Builds for all projects found." }
                        done()
                    }
                    else -> {
                        log.trace { "Fetching page $currentPage of max $maxPages" }
                        setNext(currentPage)
                    }
                }
            }
        }

        override fun feedNextPage(nextRows: List<BuildStatus>) {
            nextRows.forEach { row ->
                projectsLeft.removeIf { it.projectId == row.projectId }
            }
            log.debug { "Projects left to fetch status for: ${projectsLeft.joinToString { it.projectId.id }}" }
            currentPage++
        }

        override fun clean(rows: List<BuildStatus>) = rows
    }

    class Until(
        private val buildId: BuildId,
        private val timeoutPages: Int = 5
    ) : FetchUntil() {

        private val log = logger {}
        private var currentPage = 1
        private var buildIdFound = false

        override fun iterator(): Iterator<Int> = object : AbstractIterator<Int>() {
            override fun computeNext() {
                if (currentPage > timeoutPages) {
                    throw Exception("Was not able to find build with id: ${buildId.id} within the last $timeoutPages pages.")
                }
                if (buildIdFound) {
                    done()
                } else {
                    setNext(currentPage)
                    log.trace { "Fetching page $currentPage" }
                    currentPage++
                }
            }
        }

        override fun feedNextPage(nextRows: List<BuildStatus>) {
            buildIdFound = nextRows.any { it.buildId == buildId }
        }

        override fun clean(rows: List<BuildStatus>) = rows.subList(0, rows.indexOfFirst { it.buildId == buildId })
    }
}
