package stati.build.adapter.selenium

import mu.KotlinLogging.logger
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import stati.build.domain.BuildFetcher
import stati.build.domain.BuildStatus
import stati.domain.ProjectDefinition
import java.io.File
import java.util.concurrent.TimeUnit

data class SeleniumKonfig(
    val username: String,
    val password: String,
    val headless: Boolean
)

const val GOOGLE_BASE_URL = "https://console.cloud.google.com"

class SeleniumBuildFetcher(
    private val konfig: SeleniumKonfig,
    projects: List<ProjectDefinition>,
    ignoredSources: List<String>
) : BuildFetcher {

    companion object {
        private val systemProperty = "webdriver.gecko.driver" to "src/bin/geckodriver_mac"

        init {
            val driverPath = File(systemProperty.second)
            require(driverPath.exists()) { "Not existing: ${driverPath.canonicalPath}" }
            System.setProperty(systemProperty.first, driverPath.canonicalPath)
        }
    }

    private val log = logger {}
    private val url = GOOGLE_BASE_URL + "/cloud-build/builds?project=sandbox-ijsberen&authuser=1"
    private val parser = SeleniumParser(projects, ignoredSources)
    private val driver = FirefoxDriver(FirefoxOptions().apply {
        setHeadless(konfig.headless)
    }).apply {
        manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    private val waiter = WebDriverWait(driver, 10)

//    private val buildLogUrlTemplate = "https://storage.cloud.google.com/logscloudbuild-test/log-XXX.txt?authuser=1"
    // https://console.cloud.google.com/cloud-build/builds/58859ff0-0fc2-462b-b70c-07c10e6aa2d5?project=sandbox-ijsberen&authuser=1

    override fun fetchAll(until: FetchUntil): List<BuildStatus> {
        log.info { "fetchAll($until)" }
        driver.get(url)

        log.debug { "current URL: $driver.currentUrl" }
        if (driver.currentUrl.contains("ServiceLogin")) {
            authGoogle()
        }

        if (until is FetchUntil.All) {
            changePageSize()
        }

        val allRows = mutableListOf<BuildStatus>()
        until.forEach { index ->
            if (index != 1) {
                log.trace { "click next page -> $index" }
                val buttonNext = driver.findElement(By.cssSelector("jfk-button[pan-loading-button=\"paginateCtrl.nextPagePromise\"]"))
                driver.scrollTo(buttonNext)
                buttonNext.click()
                waiter.until {
                    !buttonNext.getAttribute("class").contains("p6n-loading-button-loading")
                }
            }
            val xpathTable = "/html/body/pan-shell/pcc-shell/cfc-panel-container/div/div/cfc-panel/div[1]/div/div[3]/cfc-panel-container/div/div/cfc-panel[1]/div/div/cfc-panel-container/div/div/cfc-panel[1]/div/div/cfc-panel-container/div/div/cfc-panel[2]/div/div/central-page-area/div/div/pcc-content-viewport/div/div/ng2-router-root/div/ng1-router-root-loader/xap-deferred-loader-outlet/ng1-router-root-wrapper/ng1-router-root/div/ng-view/section/div/div/ng-transclude/table"
            val table = driver.findElement(By.xpath(xpathTable))
            log.trace { "Parsing table ..." }
            val rows = parseTable(table)
            allRows += rows
            until.feedNextPage(rows)
        }
        return until.clean(allRows).sortedBy { it.started }
    }

    fun stop() {
        log.trace { "Quit driver." }
        driver.quit()
    }

    private fun authGoogle() {
        log.trace { "Enter email." }
        val emailInput = driver.findElement(By.xpath("//*[@id=\"identifierId\"]"))
        emailInput.click()
        emailInput.sendKeys(konfig.username)

        log.trace { "Click next." }
        val buttonNext = driver.findElement(By.xpath("//*[@id=\"identifierNext\"]"))
        buttonNext.click()

        log.trace { "Enter password." }
        val passwordLocator = By.xpath("//*[@name=\"password\"]")
        waiter.until(ExpectedConditions.elementToBeClickable(passwordLocator))
        val passwordInput = driver.findElement(passwordLocator)
        passwordInput.sendKeys(konfig.password)

        log.trace { "Click next." }
        val passwordNext = driver.findElement(By.xpath("//*[@id=\"passwordNext\"]"))
        passwordNext.click()
    }

    private val selectIndexOf200RowsPerPage = ":7"
    private fun changePageSize() {
        val selectLocator = By.cssSelector("jfk-select[ng-model=\"paginateCtrl.pageSize\"]")
        waiter.until(ExpectedConditions.elementToBeClickable(selectLocator))
        val select = driver.findElement(selectLocator)
        select.click()

        val option = select.findElement(By.cssSelector("div[role=\"menuitem\"][id=\"$selectIndexOf200RowsPerPage\"]"))
        option.click()
        Thread.sleep(3_000)
    }

    private fun parseTable(table: WebElement): List<BuildStatus> =
        table.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).mapNotNull { tr ->
            parser.parseRow(tr)
        }
}

private fun RemoteWebDriver.scrollTo(element: WebElement) {
    executeScript("arguments[0].scrollIntoView(true);", element)
    Thread.sleep(500)
}
