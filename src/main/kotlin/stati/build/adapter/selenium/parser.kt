package stati.build.adapter.selenium

import mu.KotlinLogging
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import stati.build.domain.BuildId
import stati.build.domain.BuildStatus
import stati.build.domain.BuildStatusIndicator
import stati.domain.ProjectDefinition
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

fun main() {
    val regexp = Regex("(([0-9][0-9]?) min )?([0-9][0-9]?) sec")

    listOf(
        "7 sec",
        "14 sec",
        "7 min 1 sec",
        "7 min 41 sec",
        "57 min 41 sec",
        "7 min 41 xxx",
        "7 min sec"
    ).forEach {
        val result = regexp.find(it)
        if (result == null) {
            println("$it ... NOP")
        } else {
            val min = result.groupValues[2].toIntOrNull() ?: 0
            val sec = result.groupValues[3].toInt()
            println("$it => $min min $sec sec")
        }
    }
}

class SeleniumParser(
    projects: List<ProjectDefinition>,
    private val ignoredSources: List<String>
) {

    private val log = KotlinLogging.logger {}
    private val projectIdBySourceName = projects.map { it.buildSourceName to it.projectId }.toMap()

    fun parseRow(tr: WebElement): BuildStatus? {
        val tds = tr.findElements(By.tagName("td")).toList()
        val status = tr.findElement(By.className("p6n-icon-status")).getAttribute("status")
        val source = tds[1].text
        val projectId = projectIdBySourceName[source]
        if (projectId == null) {
            if (!ignoredSources.contains(source)) {
                log.warn { "Unhandled project source name: '$source'" }
            }
            return null
        }

        val buildLink = tds[0].findElement(By.tagName("a"))
        return BuildStatus(
            /*
            0: 35d82686-c1a1-4d22-bfca-57449fa7cfc7
            1: Bitbucket ibood/phoenix
            2: 2c13c20
            3: Push to master branch
            4: Push to master branch
            5: 12 minutes ago
            6: 8 min 1 sec
            7: —
             */
//                gitCommit = tds[2].text,
            projectId = projectId,
            status = BuildStatusIndicator.of(status),
            buildId = BuildId(buildLink.getAttribute("pan-buttertip-message")),
            buildUrl = buildLink.getAttribute("href").replace("authuser=0", "authuser=1"),
            started = SeleniumDateParser.parseDate(tds[5].text),
            duration = parseDuration(tds[6].text)
        )
    }

    private val durationRegexp = Regex("(([0-9][0-9]?) min )?([0-9][0-9]?) sec")
    private fun parseDuration(text: String): Duration? {
        if (text.length == 1) return null // currently building, text == "-"
        val values = durationRegexp.find(text)?.groupValues ?: throw Exception("Unparsable duration string: '$text'")
        val min = values[2].toLongOrNull() ?: 0
        val sec = values[3].toLong()
        return Duration.ofSeconds(min * 60L + sec)
    }


}

private object SeleniumDateParser {
    private val formatTime = DateTimeFormatter.ofPattern("HH:mm")
    private val dateTime = DateTimeFormatter.ofPattern("dd/MM/yyyy, HH:mm")

    fun parseDate(string: String): LocalDateTime {
        if (string == "Just now") {
            return LocalDateTime.now()
        }
        if (string.endsWith(" minute ago") || string.endsWith(" minutes ago")) {
            val minutesAgo = string.substring(0, string.indexOf(" ")).toLong()
            return LocalDateTime.now().minusMinutes(minutesAgo)
        }
        if (string.length == 5) {
            val time = LocalTime.parse(string, formatTime)
            return LocalDateTime.now().withHour(time.hour).withMinute(time.minute).withSecond(0)
        }
        return LocalDateTime.parse(string, dateTime)
    }
}

private fun BuildStatusIndicator.Companion.of(status: String) = when (status) {
    "success" -> BuildStatusIndicator.Success
    "error" -> BuildStatusIndicator.Failing
    "working" -> BuildStatusIndicator.Building
    "stopped" -> BuildStatusIndicator.Stopped
    else -> throw Exception("Unhandled build row status: '$status'!")
}
