package stati.domain

import stati.build.domain.Url
import java.time.Duration
import java.time.LocalDateTime

const val UNAVAILABLE_SYMBOL = "N/A"

interface IProjectMap<V> : Iterable<Pair<ProjectId, V>> {
    val map: Map<ProjectId, V>

    operator fun get(id: ProjectId): V
}

interface IMutableProjectMap<V> : IProjectMap<V> {
    operator fun set(id: ProjectId, value: V)
}

class ProjectMap<V> private constructor(initialMap: Map<ProjectId, V>) : IMutableProjectMap<V> {

    companion object {
        fun <X : HasProjectId> build(initialMap: Map<ProjectId, X>): IProjectMap<X> = ProjectMap(
            initialMap
        )

        fun <X> buildById(initialMap: Map<ProjectId, X>): IProjectMap<X> = ProjectMap(
            initialMap
        )

        fun <X : HasProjectId> build(initialData: List<X>): IProjectMap<X> = ProjectMap(
            initialData.map { it.projectId to it }.toMap()
        )

        fun <X : HasProjectId> buildEmpty(): IProjectMap<X> = ProjectMap(
            emptyMap()
        )
    }

    private val _map = initialMap.toMutableMap()
    override val map: Map<ProjectId, V> = _map

    override operator fun get(id: ProjectId): V =
        _map[id] ?: throw Exception("Unknown project ID: $id")

    override operator fun set(id: ProjectId, value: V) {
        _map[id] = value
    }

    override fun iterator(): Iterator<Pair<ProjectId, V>> =
        _map.entries.map { it.key to it.value }.iterator()
}

data class ProjectId(val id: String) {
    override fun toString() = id
}

interface HasProjectId {
    val projectId: ProjectId
}

data class ProjectStatus(
    override val projectId: ProjectId,
    val label: String,
    val indicator: ProjectStatusIndicator,
    val buildLastSuccess: LocalDateTime?,
    val buildDuration: Duration?,
//    val buildLastFailure: LocalDateTime?
    val buildUrl: Url?
) : HasProjectId

data class ProjectDefinition(
    override val projectId: ProjectId,
    val buildSourceName: String,
    val label: String,
    val gitUri: String
) : HasProjectId {

    constructor(
        projectId: String,
        buildSourceName: String = "Bitbucket ibood/$projectId",
        label: String,
        gitUri: String = "https://bitbucket.org/ibood/$projectId.git"
    ) : this(
        projectId = ProjectId(projectId),
        buildSourceName = buildSourceName,
        label = label,
        gitUri = gitUri
    )
}

enum class ProjectStatusIndicator {
    OK,
    FAIL,
    UNKNOWN
}
