package stati.domain

interface NotificationNotifier {
    fun notify(message: String)
}

interface GitView {
    fun allCommittersSince(project: ProjectDefinition, sinceCommit: CommitId): List<Committer>
}

data class CommitId(val id: String)

data class Committer(
    val email: String
)

interface EmailSender {
    fun send(email: Email)
}

data class Email(
    val receivers: List<String>,
    val subject: String,
    val body: String
)