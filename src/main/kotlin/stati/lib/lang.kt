package stati.lib

import java.time.Duration

val <K, V> Map<K, V?>.isAnyValueNull: Boolean get() = values.any { it == null }

fun Duration.toPrettyPrint(): String {
    return "${toMinutes()} min"
}

fun <E> List<E>.takeWhileInclusive(predicate: (E) -> Boolean): List<E> {
    val result = mutableListOf<E>()
    forEach {
        if (predicate.invoke(it)) {
            result += it
        } else {
            result += it
            return result
        }
    }
    return result
}
