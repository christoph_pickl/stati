package stati.adapter

import mu.KotlinLogging.logger
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton
import stati.KodeinModules
import stati.Konfig
import stati.adapter.git.JGitView
import stati.domain.EmailSender
import stati.domain.GitView
import stati.domain.NotificationNotifier
import java.io.File

private val log = logger {}

@Suppress("unused") fun KodeinModules.adapterModule(konfig: Konfig) = Kodein.Module("Adapter Module") {
    bind<NotificationNotifier>() with singleton { GrowlNotificationNotifier() }
    bind<GitView>() with singleton { JGitView(File(STATI_HOME_DIRECTORY, "gitRepos"), konfig.gitUsername, konfig.gitPassword) }
    bind<EmailSender>() with singleton {
        if (konfig.enableMailSending) {
            SmtpEmailSender(konfig.mailConfig)
        } else {
            log.warn { "[DEVELOP] Binding ${DummyEmailSender::class.simpleName}" }
            DummyEmailSender
        }
    }
}
