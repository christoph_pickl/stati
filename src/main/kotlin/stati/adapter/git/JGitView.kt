package stati.adapter.git

import mu.KotlinLogging.logger
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import stati.adapter.STATI_HOME_DIRECTORY
import stati.domain.CommitId
import stati.domain.Committer
import stati.domain.GitView
import stati.domain.ProjectDefinition
import stati.lib.takeWhileInclusive
import java.io.File

fun main() {
    val aloha = ProjectDefinition(
        projectId = "aloha",
        label = "Aloha"
    )
    val git = JGitView(
        rootDirectory = File(STATI_HOME_DIRECTORY, "gitRepos"),
        username = System.getProperty("stati.git_username"),
        password = System.getProperty("stati.git_password")
    )
    // df49bdfadd59547f088f7d80de212eccffad6e71
    // c951b92eaebffc79da6085eb46d8cbc188c1f91b
    // 22d9591a872a47126608be280d72a5ff267aa8a5
    println(git.allCommittersSince(aloha, CommitId("22d9591a872a47126608be280d72a5ff267aa8a5")).joinToString("\n"))
}

// https://www.baeldung.com/jgit
// https://git-scm.com/book/uz/v2/Appendix-B%3A-Embedding-Git-in-your-Applications-JGit
// http://www.eclipse.org/jgit/
class JGitView(
    private val rootDirectory: File,
    private val username: String,
    password: String
) : GitView {

    private val log = logger {}
    private val credentials = UsernamePasswordCredentialsProvider(username, password)

    override fun allCommittersSince(project: ProjectDefinition, sinceCommit: CommitId): List<Committer> {
        log.debug { "Connecting to GIT as user: $username" }
        val git = if (!project.gitDirectory.exists()) {
            log.debug { "Cloning repository: ${project.gitUri}" }
            Git.cloneRepository()
                .setCredentialsProvider(credentials)
                .setURI(project.gitUri)
                .setDirectory(project.gitDirectory)
                .call()
        } else {
            log.debug { "Pulling repository: ${project.gitUri}" }
            Git.open(project.gitDirectory).apply {
                val result = pull().setCredentialsProvider(credentials).call()
                log.trace { "GIT pull result: $result" }
                if (!result.isSuccessful) {
                    throw Exception("Pull for ${project.projectId} failed: $result")
                }
            }
        }

        val commits = fetchCommits(git)

        val consideredCommits = commits.takeWhileInclusive { it.id.name != sinceCommit.id }
//        consideredCommits.forEach { commit ->
//            println("${commit.id.name} -> ${commit.committerIdent.emailAddress} (${commit.shortMessage})")
//        }
        return consideredCommits.map { it.committerIdent.emailAddress }.distinct().map { Committer(it) }
    }

    private fun fetchCommits(git: Git): List<RevCommit> {
        return git.log()
//            .addRange(git.repository.resolve(Constants.HEAD), RevCommit.fromString(commitId.id))
            .add(git.repository.resolve("refs/heads/master"))
            .call().toList()

        // git rev-list master

//        val walk = RevWalk(git.repository)
//        walk.markStart(walk.parseCommit(git.repository.resolve(Constants.HEAD)))
//        walk.sort(RevSort.COMMIT_TIME_DESC)
//        walk.revFilter = RevFilter()??
//        walk.close()
    }

    private val ProjectDefinition.gitDirectory get() = File((rootDirectory), projectId.id)

}
