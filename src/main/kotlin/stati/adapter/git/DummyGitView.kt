package stati.adapter.git

import stati.domain.CommitId
import stati.domain.Committer
import stati.domain.GitView
import stati.domain.ProjectDefinition

class DummyGitView : GitView {
    override fun allCommittersSince(project: ProjectDefinition, sinceCommit: CommitId): List<Committer> {
        return listOf(Committer("christoph.pickl@ibood.com"))
    }
}
