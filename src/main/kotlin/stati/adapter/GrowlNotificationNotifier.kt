package stati.adapter

import stati.domain.NotificationNotifier

class GrowlNotificationNotifier(
    private val title: String = "Stati Notification"
) : NotificationNotifier {

    override fun notify(message: String) {
        val builder = ProcessBuilder("/usr/bin/osascript", "-e", "display notification \"$message\" with title \"$title\"")
        builder.inheritIO()
        val process = builder.start()
        val exitCode = process.waitFor()
        if (exitCode != 0) {
            throw Exception("Failed to execute growl osa script! (exit code: $exitCode)")
        }
    }
}
