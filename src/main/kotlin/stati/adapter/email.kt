package stati.adapter

import com.sun.mail.smtp.SMTPTransport
import mu.KotlinLogging
import stati.domain.Email
import stati.domain.EmailSender
import java.util.Properties
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

fun main() {
    val sender = SmtpEmailSender(MailConfig(
        smtpServer = "smtp.gmail.com",
        username = System.getProperty("stati.mail_username"),
        password = System.getProperty("stati.mail_password"),
        port = 587
    ))
    println("Sending mail ...")
    sender.send(Email(listOf("christoph.pickl@ibood.com"), "my subj", "my body"))
    println("DONE")
}

object DummyEmailSender : EmailSender {
    override fun send(email: Email) {
        println("NOT sending $email")
    }
}

data class MailConfig(
    val smtpServer: String,
    val username: String,
    val password: String,
    val port: Int
)

class SmtpEmailSender(
    private val config: MailConfig
) : EmailSender {

    private val log = KotlinLogging.logger {}
    override fun send(email: Email) {
        val session = javax.mail.Session.getDefaultInstance(Properties().apply {
            put("mail.transport.protocol", "smtp")
            put("mail.smtp.starttls.enable", true)
            put("mail.smtp.auth", "true")
            put("mail.smtp.host", config.smtpServer)
            put("mail.smtp.port", config.port)
        })
        val message = email.toMessage(session)
        val smtp = session.getTransport("smtp") as SMTPTransport
        log.debug { "Sending $email ..." }
        smtp.connect(config.smtpServer, config.username, config.password)
        smtp.sendMessage(message, message.allRecipients)
        log.trace { "Response: ${smtp.lastServerResponse}" }
        smtp.close()
    }

    private fun Email.toMessage(session: Session): javax.mail.Message {
        val email = MimeMessage(session)
        email.setFrom(InternetAddress(config.username))
        email.addRecipients(javax.mail.Message.RecipientType.TO, receivers.map { InternetAddress(it) }.toTypedArray())
        email.subject = subject
        email.setText(body)
        return email
    }
}
