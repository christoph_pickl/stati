package stati

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.tornadofx.installTornadoSource
import stati.adapter.adapterModule
import stati.build.buildModule
import stati.fx.StatiFxApp
import stati.fx.viewModule
import tornadofx.launch

object StatiApp {
    @JvmStatic
    fun main(args: Array<String>) {
        launch<StatiFxApp>(args)
    }
}

object KodeinModules // register modules via extension properties

fun appKodein() = Kodein {
    val konfig = Konfig.production
    bind<Konfig>() with instance(konfig)
    import(KodeinModules.adapterModule(konfig))
    import(KodeinModules.buildModule(konfig))
    import(KodeinModules.viewModule())
    installTornadoSource()
}
