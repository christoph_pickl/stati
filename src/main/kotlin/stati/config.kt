package stati

import stati.adapter.MailConfig
import stati.domain.ProjectDefinition
import stati.domain.ProjectId
import java.time.Duration

data class Konfig(
    val projects: List<ProjectDefinition>,
    val refreshDelay: Duration,
    val ignoredBuildSources: List<String>,

    val enableMailSending: Boolean,
    val enableSeleniumBuildFetcher: Boolean,
    val seleniumHeadless: Boolean,
    val googleUsername: String,
    val googlePassword: String,
    val gitUsername: String,
    val gitPassword: String,
    val mailConfig: MailConfig
) {
    companion object {

        val aloha = ProjectDefinition(
            projectId = "aloha",
            label = "Aloha"
        )

        val production = Konfig(
            enableMailSending = false,
            enableSeleniumBuildFetcher = true,
            seleniumHeadless = true,

            refreshDelay = Duration.ofMinutes(10),

            googleUsername = System.getProperty("stati.google_username") ?: throw Exception("no stati.google_username"),
            googlePassword = System.getProperty("stati.google_password") ?: throw Exception("no stati.google_password"),

            gitUsername = System.getProperty("stati.git_username") ?: throw Exception("no stati.git_username"),
            gitPassword = System.getProperty("stati.git_password") ?: throw Exception("no stati.git_password"),

            mailConfig = MailConfig(
                smtpServer = "smtp.gmail.com",
                username = System.getProperty("stati.mail_username"),
                password = System.getProperty("stati.mail_password"),
                port = 587
            ),

            ignoredBuildSources = listOf("Bitbucket ibood/phoenix",
                "gs://sandbox-ijsberen_cloudbuild/source/1572877757.43-c6abcb40408d45a7bce3f7d7c0198c7e.tgz",
                "gs://sandbox-ijsberen_cloudbuild/source/1572877730.16-e1c3f845938f47cfad3e84dedd6a8975.tgz",
                "gs://sandbox-ijsberen_cloudbuild/source/1572877662.22-4fb14de9741746c3ad173448e1481c3d.tgz",
                "gs://sandbox-ijsberen_cloudbuild/source/1572875984.58-39a220aaef5a4316b7a58e19510996c7.tgz"
            ),
            projects = listOf(
                aloha,
                ProjectDefinition(
                    projectId = "antenna",
                    label = "Antenna"
                ),
                ProjectDefinition(
                    projectId = "cupid",
                    label = "Cupid"
                ),
                ProjectDefinition(
                    projectId = "gondola",
                    label = "Gondola"
                ),
                ProjectDefinition(
                    projectId = "tictac",
                    label = "TicTac"
                ),
                ProjectDefinition(
                    projectId = "maestro",
                    label = "Maestro"
                ),
                ProjectDefinition(
                    projectId = "panda",
                    label = "Panda"
                ),
                ProjectDefinition(
                    projectId = "subway",
                    label = "Subway"
                ),
                ProjectDefinition(
                    projectId = "zigzag",
                    label = "ZigZag"
                )
            ).sortedBy { it.label }
        )
    }

    private val projectsById = projects.associateBy { it.projectId }

    fun projectById(id: ProjectId) = projectsById[id]!!
}
