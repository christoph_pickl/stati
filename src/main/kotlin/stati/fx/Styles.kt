package stati.fx

import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.cssclass

object Colors {
    object ProjectView {
        private val opacity = 0.9
        val initial = Color(0.3, 0.3, 0.3, opacity)
        val ok = Color(0.0, 0.7, 0.0, opacity)
        val fail = Color(0.7, 0.0, 0.0, opacity)
        val unknown = Color(0.4, 0.4, 0.4, opacity)
    }
}

class Styles : Stylesheet() {
    companion object {
        val testChart by cssclass()
    }

    init {
        testChart {
            defaultColor0 {
                barFill = Color.GREEN
            }
            defaultColor1 {
                barFill = Color.GREY
            }
            defaultColor2 {
                barFill = Color.RED
            }
        }
        scrollPane {
            backgroundColor += Color.TRANSPARENT
            viewport {
                backgroundColor += Color.TRANSPARENT
            }
        }
    }
}
