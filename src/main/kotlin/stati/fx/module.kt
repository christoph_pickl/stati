package stati.fx

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import org.kodein.di.generic.instance
import stati.KodeinModules
import stati.fx.controller.ApplicationState
import stati.fx.controller.MainController
import stati.fx.controller.RefreshController

@Suppress("unused") fun KodeinModules.viewModule() = Kodein.Module("FX Module") {
    bind() from eagerSingleton { MainController() }
    bind() from eagerSingleton { RefreshController() }
    bind() from instance(ApplicationState())
}
