package stati.fx

import javafx.scene.text.Font
import javafx.scene.text.Text

fun Text.withFont(font: Font) = apply {
    this.font = font
}

//class PaintBinding(paintValue: Paint) : ObjectBinding<Paint>() {
//
//    var paintValue: Paint
//        get() = this.value
//    set(value) {
//        if (this.value != value) {
//            this.value = value
//        }
//    }
//    override fun computeValue(): Paint = paintValue
//
//}
//
