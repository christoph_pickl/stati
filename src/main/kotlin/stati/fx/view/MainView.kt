package stati.fx.view

import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.NumberAxis
import javafx.scene.image.Image
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundImage
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import mu.KotlinLogging.logger
import org.kodein.di.generic.instance
import org.kodein.di.tornadofx.kodein
import stati.Konfig
import stati.domain.HasProjectId
import stati.domain.IProjectMap
import stati.domain.ProjectMap
import stati.domain.ProjectStatusIndicator
import stati.domain.UNAVAILABLE_SYMBOL
import stati.fx.Colors
import stati.fx.ProjectStatiLoadedEvent
import stati.fx.RefreshEvent
import stati.fx.Styles
import stati.fx.controller.ApplicationState
import stati.lib.toPrettyPrint
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.button
import tornadofx.data
import tornadofx.enableWhen
import tornadofx.hbox
import tornadofx.scrollpane
import tornadofx.series
import tornadofx.stackedbarchart
import tornadofx.stringBinding
import tornadofx.vbox
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit


class MainView : View() {

    private val logg = logger {}
    private val state: ApplicationState by kodein().instance()
    private val refreshRunning by lazy { state.refreshRunning }

    private val projectViews = Konfig.production.projects.map {
        it to ProjectView(label = it.label)
    }.toProjectMap()

    init {
        titleProperty.bind(refreshRunning.stringBinding { "iBOOD Stati" + if (it!!) " - Loading ..." else "" })
        subscribe<ProjectStatiLoadedEvent> { event ->
            logg.info { "on ProjectStatiLoadedEvent" }
            val now = LocalDateTime.now()
            event.stati.forEach {
                val view = projectViews[it.first]
                val status = it.second

                view.statusColorBinding.value = status.indicator.toColor()
                view.lastSuccess.value = "Last success: ${status.buildLastSuccess?.toTimeAgo(now)
                    ?: UNAVAILABLE_SYMBOL}"
                view.duration.value = "Duration: ${status.buildDuration?.toPrettyPrint() ?: UNAVAILABLE_SYMBOL}"
                view.buildUrl.value = status.buildUrl
            }
        }
    }

    override val root = hbox {
        background = Background(BackgroundImage(
            Image("/stati/ibood_logo.png"), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT
        ))
        scrollpane {
            vbox(spacing = 15.0) {
                alignment = Pos.CENTER
                padding = Insets(15.0)
                add(MultiRowBox(projectViews.map.values.toList()))
                button("Refresh").apply {
                    enableWhen(refreshRunning.not())
                    prefWidth = 150.0
                    action {
                        fire(RefreshEvent)
                    }
                }
            }
        }
    }
}

private class MultiRowBox(children: List<View>) : View() {

    private val maxItemsInRow = 3
    private val spacing = 10.0

    override val root = vbox(spacing = spacing) {
        val rows = children.size / maxItemsInRow + if (children.size % maxItemsInRow == 0) 0 else 1
        var currentChildIndex = -1
        repeat(1.rangeTo(rows).count()) {
            hbox(spacing = spacing) {
                repeat(1.rangeTo(maxItemsInRow).count()) {
                    currentChildIndex++
                    add(children[currentChildIndex])
                }
            }
        }
    }
}

private fun VBox.dummyChart() {
    // https://github.com/edvin/tornadofx/wiki/Charts
    // https://github.com/edvin/tornadofx-guide/blob/master/part1/8.%20Charts.md
    stackedbarchart("Tests", CategoryAxis(), NumberAxis()) {
        addClass(Styles.testChart)
//                    hgrow = Priority.ALWAYS
//                    vgrow = Priority.ALWAYS
        series("Success") {
            //.background
            data("#1", 42)
            data("#2", 45)
            data("#3", 61)
//                        this.chart.background = Background(BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY))
        }
        series("Ignored") {
            data("#1", 0)
            data("#2", 1)
            data("#3", 1)
        }
        series("Failing") {
            data("#1", 0)
            data("#2", 2)
            data("#3", 0)
        }
    }
}


fun <P : HasProjectId, V> Collection<Pair<P, V>>.toProjectMap(): IProjectMap<V> = ProjectMap.buildById(
    // could add a check for duplicate projectIds set
    map { it.first.projectId to it.second }.toMap()
)

fun LocalDateTime.toTimeAgo(pivot: LocalDateTime): String {
    timeSingularPlurarLabel(ChronoUnit.DAYS.between(this, pivot), "day") { return it }
    timeSingularPlurarLabel(ChronoUnit.HOURS.between(this, pivot), "hour") { return it }
    timeSingularPlurarLabel(ChronoUnit.MINUTES.between(this, pivot), "min") { return it }
    return "now"
}

private inline fun timeSingularPlurarLabel(count: Long, label: String, exit: (String) -> Unit) {
    if (count > 0) {
        return if (count == 1L) {
            exit("$count $label")
        } else {
            exit("$count ${label}s")
        }
    }
}

private fun ProjectStatusIndicator.toColor(): Color = when (this) {
    ProjectStatusIndicator.OK -> Colors.ProjectView.ok
    ProjectStatusIndicator.FAIL -> Colors.ProjectView.fail
    ProjectStatusIndicator.UNKNOWN -> Colors.ProjectView.unknown
}
