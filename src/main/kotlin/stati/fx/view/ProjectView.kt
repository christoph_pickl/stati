package stati.fx.view

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.paint.Paint
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import stati.fx.Colors
import stati.fx.withFont
import tornadofx.View
import tornadofx.action
import tornadofx.enableWhen
import tornadofx.hyperlink
import tornadofx.rectangle
import tornadofx.stackpane
import tornadofx.text
import tornadofx.vbox
import java.awt.Desktop
import java.net.URI

class ProjectView(
    private val label: String
) : View() {
    companion object {
        private val labelFont = Font.font("Verdana", FontWeight.BOLD, 18.0)
        private val detailsFont = Font.font("Verdana", FontWeight.NORMAL, 11.0)
        private val rectangleSize = 220.0 to 150.0
    }

    val statusColorBinding = SimpleObjectProperty<Paint>(Colors.ProjectView.initial)
    val lastSuccess = SimpleStringProperty()
    val duration = SimpleStringProperty()
    val buildUrl = SimpleStringProperty()

    override val root = stackpane {
        rectangle(width = rectangleSize.first, height = rectangleSize.second) {
            fillProperty().bind(statusColorBinding)
            arcWidth = 20.0
            arcHeight = 20.0
        }
        vbox(alignment = Pos.CENTER) {
            isFillWidth = true
            text(label).withFont(labelFont)
            hyperlink("GoTo Build") {
                enableWhen { buildUrl.isNotNull.and(buildUrl.isNotEmpty) }
                action {
                    Desktop.getDesktop().browse(URI.create(buildUrl.value))
                }
            }
            text(lastSuccess).withFont(detailsFont)
            text(duration).withFont(detailsFont)
        }
    }
}
