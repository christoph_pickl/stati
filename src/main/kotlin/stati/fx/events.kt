package stati.fx

import stati.build.domain.BuildReportChange
import stati.domain.IProjectMap
import stati.domain.ProjectStatus
import tornadofx.FXEvent

object RefreshEvent : FXEvent()

class ProjectStatiLoadedEvent(val stati: IProjectMap<ProjectStatus>) : FXEvent()

class ProjectsChangedEvent(val stati: List<BuildReportChange>) : FXEvent() {
    init {
        require(stati.isNotEmpty())
    }
}
