package stati.fx.controller

import mu.KotlinLogging.logger
import org.kodein.di.generic.instance
import org.kodein.di.tornadofx.kodein
import stati.Konfig
import stati.fx.RefreshEvent
import tornadofx.Controller
import java.util.Timer
import java.util.TimerTask
import kotlin.concurrent.scheduleAtFixedRate

class RefreshController : Controller() {

    private val logg = logger {}
    private val konfig: Konfig by kodein().instance()
    private val delay by lazy { konfig.refreshDelay }
    private var task: TimerTask? = null

    fun start() {
        val timer = Timer("Refresh Timer")
        logg.info { "Starting refresh task." }
        task = timer.scheduleAtFixedRate(0, delay.toMillis()) {
            fire(RefreshEvent)
            logg.debug { "Going to sleep for ${delay.toMinutes()} min" }
        }
    }

    fun stop() {
        task?.cancel()
    }
}
