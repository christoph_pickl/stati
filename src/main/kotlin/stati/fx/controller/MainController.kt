package stati.fx.controller

import javafx.beans.property.SimpleBooleanProperty
import mu.KotlinLogging.logger
import org.kodein.di.generic.instance
import org.kodein.di.tornadofx.kodein
import stati.Konfig
import stati.build.domain.BuildReport
import stati.build.domain.BuildService
import stati.build.domain.BuildStatusIndicator
import stati.domain.IProjectMap
import stati.domain.ProjectMap
import stati.domain.ProjectStatus
import stati.domain.ProjectStatusIndicator
import stati.fx.ProjectStatiLoadedEvent
import stati.fx.ProjectsChangedEvent
import stati.fx.RefreshEvent
import tornadofx.Controller
import tornadofx.finally

class ApplicationState {
    val refreshRunning = SimpleBooleanProperty(false)
}

class MainController : Controller() {

    private val logg = logger {}
    private val konfig: Konfig by kodein().instance()
    private val state: ApplicationState by kodein().instance()
    private val buildService: BuildService by kodein().instance()
    private val refreshRunning by lazy { state.refreshRunning }

    init {
        subscribe<RefreshEvent> {
            logg.info { "on RefreshEvent" }
            doRefresh()
        }
    }

    private fun doRefresh() {
        if (refreshRunning.value) {
            logg.warn { "Refresh already running, ignoring." }
            return
        }
        refreshRunning.value = true
        logg.info { "doRefresh()" }

        runAsync {
            buildService.fetchReport()
        }.apply {
            finally {
                refreshRunning.value = false
            }
        } ui { buildResult ->
            fire(ProjectStatiLoadedEvent(stati = mergeProjectStati(buildResult)))
            if (buildResult.changed.isNotEmpty()) {
                fire(ProjectsChangedEvent(buildResult.changed))
            }
        }
    }

    private fun mergeProjectStati(buildReport: BuildReport): IProjectMap<ProjectStatus> = ProjectMap.build(
        buildReport.stati.map { statusX ->
            val projectDef = konfig.projectById(statusX.projectId)
            ProjectStatus(
                projectId = statusX.projectId,
                label = projectDef.label,
                indicator = statusX.status.toIndicator(),
                buildLastSuccess = statusX.lastSuccess,
                buildDuration = statusX.duration,
                buildUrl = statusX.buildUrl
            )
        }
    )

    private fun BuildStatusIndicator.toIndicator() = when (this) {
        BuildStatusIndicator.Success -> ProjectStatusIndicator.OK
        BuildStatusIndicator.Building -> ProjectStatusIndicator.OK
        BuildStatusIndicator.Failing -> ProjectStatusIndicator.FAIL
        BuildStatusIndicator.Unknown -> ProjectStatusIndicator.UNKNOWN
        BuildStatusIndicator.Stopped -> ProjectStatusIndicator.UNKNOWN // TODO here we should just check the previous build state
    }

}
