package stati.fx

import javafx.stage.Stage
import mu.KotlinLogging.logger
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import stati.appKodein
import stati.build.adapter.selenium.SeleniumBuildFetcher
import stati.build.domain.BuildFetcher
import stati.fx.controller.RefreshController
import stati.fx.view.MainView
import tornadofx.App

class StatiFxApp : App(
    primaryView = MainView::class,
    stylesheet = Styles::class
), KodeinAware {

    private val log = logger {}
    override var kodein = appKodein()
    private val refreshController by kodein.instance<RefreshController>()
    private val selenium by kodein.instance<BuildFetcher>()

    override fun start(stage: Stage) {
        log.info { "start()" }
        super.start(stage)
        refreshController.start()
        stage.sizeToScene()
    }

    override fun stop() {
        super.stop()
        log.debug { "Stop invoked." }
        refreshController.stop()
        (selenium as? SeleniumBuildFetcher)?.also {
            it.stop()
        }
    }
}
