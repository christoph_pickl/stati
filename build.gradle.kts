import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
    jcenter()
    mavenCentral()
    maven { setUrl("https://dl.bintray.com/kodein-framework/Kodein-DI") }
}


plugins {
    kotlin("jvm") version "1.3.50"
}


dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("no.tornado:tornadofx:1.7.19") {
        exclude(group = "org.jetbrains.kotlin", module = "kotlin-stdlib-jdk7")
    }
    implementation("org.kodein.di:kodein-di-generic-jvm:6.3.3")
    implementation("org.kodein.di:kodein-di-framework-tornadofx-jvm:6.3.3")
    implementation("io.github.microutils:kotlin-logging:1.7.6")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("org.eclipse.jgit:org.eclipse.jgit:5.5.1.201910021850-r")

    implementation("javax.mail:mail:1.4.7")

    implementation("org.seleniumhq.selenium:selenium-java:3.14.0")
//    implementation("com.google.api-client:google-api-client:1.30.2")
    implementation("com.google.apis:google-api-services-cloudbuild:v1-rev863-1.25.0")
    implementation("com.google.guava:guava:28.1-jre")
}



tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }
}
